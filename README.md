[MyBinder configuration](https://mybinder.readthedocs.io/en/latest/using/config_files.html)
===

- [`environment.yml`](https://mybinder.readthedocs.io/en/latest/using/config_files.html#environment-yml-install-a-conda-environment)
  - Seems to be better-supported
  - Overrides other files (see documentation)
- [`apt.txt`](https://mybinder.readthedocs.io/en/latest/using/config_files.html#apt-txt-install-packages-with-apt-get)
- [`requirements.txt`](https://mybinder.readthedocs.io/en/latest/using/config_files.html#requirements-txt-install-a-python-environment)
- [`runtime.txt`](https://mybinder.readthedocs.io/en/latest/using/config_files.html#runtime-txt-specifying-runtimes)
  - This file does not seem to work. Replaced with `environment.yml`.
